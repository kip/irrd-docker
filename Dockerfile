FROM pypy:3.9-slim-bullseye
ARG IRRD_VERSION=4.2.6
RUN apt update && apt install -y g++ gcc libpq-dev && rm -rf /var/lib/apt/lists/*
RUN pip3 install --disable-pip-version-check --no-cache-dir irrd==$IRRD_VERSION
RUN apt purge --auto-remove -y g++ gcc
RUN useradd -ms /bin/bash irrd
COPY ./docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
